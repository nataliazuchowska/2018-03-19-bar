package pl.codementors.bar;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;

public class Client implements Runnable {

    private static final Logger log = Logger.getLogger(Barman.class.getCanonicalName());

    private boolean running = true;

    public void stop() {
        running = false;
    }
    /**
     * Pomocnicza zmienna dla id drinka z ArrayList.
     */
    private int currentDrinkId = -1;

    /**
     * Nazwa drinka którego może pić klient.
     */
    private final String drinkName;

    private String clientName;

    /**
     * Współdzielona lista zamówionych drinków.
     */
    private final ArrayList<Drink> orderQueue;

    public Client(ArrayList<Drink> orderQueue, String drinkName, String clientName) {
        this.orderQueue = orderQueue;
        this.drinkName = drinkName;
        this.clientName = clientName;
    }

    /**
     * Czeka na drinka. Jeżeli barman poinformuje o tym że jakiś
     * jest gotowy sprawdza czy to jego drink. Jeżeli tak to go pije
     * przez tyle sekund ile ma liter w nazwie.
     */
    @Override
    public void run() {
        while (running) {
            try {
                int queueSize = checkOrderQueueSize();

                /**
                 * Dopóki mamy szansę na coś do picia to czekamy i jeżeli możemy
                 * to pijemy.
                 */
                while (0 < queueSize) {
                    Drink drink = waitForBarmanDrinkNotification();

                    if (drink != null) {
                        drink(drink);
                    }

                    queueSize = checkOrderQueueSize();
                }
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
            System.out.println("End of client " + clientName + " waiting for " + drinkName + " thread");

    }

    /**
     * Sprawdzamy na liście czy mamy gotowego drinka którego możemy wypić.
     *
     * @param name
     * @return
     */
    private Drink findDrink(String name) {
        for (int i = 0; i < orderQueue.size(); i++) {
            Drink nextDrink = orderQueue.get(i);

            if (nextDrink.name().equals(name) && nextDrink.isReady()) {
                currentDrinkId = i;
                return nextDrink;
            }
        }

        currentDrinkId = -1;
        return null;
    }

    /**
     * Sprawdzamy ile jest wszystkich drinków - do zrobienia + zrobionych.
     *
     * @return
     */
    private int checkOrderQueueSize() {
        synchronized (orderQueue) {
            return orderQueue.size();
        }
    }

    /**
     * Czekamy na powiadomienie że jakikolwiek drink jest gotowy.
     * Gdy drink jest gotowy sprawdzamy czy to nasz drink.
     *
     * @return
     * @throws InterruptedException
     */
    private Drink waitForBarmanDrinkNotification() throws InterruptedException{
        synchronized (orderQueue) {
            orderQueue.wait();

            return findDrink(drinkName);
        }
    }

    /**
     * Usuwamy drinka z listy drinków.
     *
     * @param idFromList
     * @param drinkName
     */
    private void removeDrinkFromQueue(int idFromList, String drinkName) {
        synchronized (orderQueue) {
            System.out.println(idFromList + " - " + drinkName + " drinked " + "by " + clientName);
            orderQueue.remove(idFromList);
        }
    }

    /**
     * Pijemy drinka tyle sekund ile ma liter i usuwamy go z listy.
     *
     * @param drink
     * @throws InterruptedException
     */
    private void drink(Drink drink) throws InterruptedException {
        int drinkNameLenght = drink.name().length();
        Thread.sleep(drinkNameLenght * 1000);
        removeDrinkFromQueue(currentDrinkId, drink.name());
    }
}
