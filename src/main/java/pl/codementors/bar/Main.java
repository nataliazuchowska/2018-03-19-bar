package pl.codementors.bar;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList<Drink> orderQueue = new ArrayList<>();
        orderQueue.add(new Drink("Foo"));
        orderQueue.add(new Drink("BarBaz"));
        orderQueue.add(new Drink("Mochito"));
        orderQueue.add(new Drink("Chupachups"));
        orderQueue.add(new Drink("Monte"));
        orderQueue.add(new Drink("AngryDog"));


        Barman barman = new Barman(orderQueue);
        Client drunkGayClient = new Client(orderQueue, "Foo", "John");
        Client businessmanClient = new Client(orderQueue, "BarBaz", "Peter");
        Client poorClient = new Client(orderQueue, "Mochito", "Michael");
        Client richClient = new Client(orderQueue, "Chupachups", "Anna");
        Client happyClient = new Client(orderQueue, "Monte", "Ben");

       Thread thread1 = new Thread(drunkGayClient);
       Thread thread2 = new Thread(businessmanClient);
       Thread thread3 =new Thread(poorClient);
       Thread thread4 =  new Thread(richClient);
       Thread thread5 = new Thread(happyClient);
       Thread thread6 = new Thread(barman);

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
        thread6.start();


      Scanner scanner = new Scanner(System.in);
        while (true) {
            String line = scanner.nextLine();
            if (line.equals("quit")) {
                break;
            }

        }
            barman.stop();
            drunkGayClient.stop();
            businessmanClient.stop();
            poorClient.stop();
            richClient.stop();
            happyClient.stop();

            thread1.interrupt();
            thread2.interrupt();
            thread3.interrupt();
            thread4.interrupt();
            thread5.interrupt();
            thread6.interrupt();



    }
}
