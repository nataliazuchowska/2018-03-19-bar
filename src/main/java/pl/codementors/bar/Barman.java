package pl.codementors.bar;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Barman implements Runnable {


    private static final Logger log = Logger.getLogger(Barman.class.getCanonicalName());

    private boolean running = true;

    public void stop() {
        running = false;
    }
    /**
     * Pomocnicza zmienna dla id drinka z ArrayList.
     */
    private int currentDrinkId = -1;

    /**
     * Współdzielona lista zamówionych drinków.
     */
    private final ArrayList<Drink> orderQueue;

    public Barman(ArrayList<Drink> orderQueue) {
        this.orderQueue = orderQueue;
    }

    /**
     * Robi co sekundę drinka. Po jego zrobieniu
     * informuje o tym wszystkich klientów.
     */
    @Override
    public void run() {
        while (running) {
            try {
                int orderQueueSize = checkOrderQueueSize();

                while (0 < orderQueueSize) {
                    takeDrink();
                    orderQueueSize = checkOrderQueueSize();
                }
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }

            synchronized (orderQueue) {
                orderQueue.notifyAll();
            }
        }
            System.out.println("End of barman thread");

    }

    /**
     * Czy robi drinka czy nie czeka 4 sekundy.
     *
     * @throws InterruptedException
     */
    public String takeDrink() throws InterruptedException {
        Thread.sleep(4000);
        String drinkName = null;

        synchronized (orderQueue) {
            Drink drink = findNotReadyDrink();

            if (drink != null) {
                drink.markAsReady();
                orderQueue.set(currentDrinkId, drink);
                drinkName = drink.name();
                System.out.println(currentDrinkId + " - " + drink.name() + " is ready!");
                orderQueue.notifyAll();
            }
        }
        return drinkName;
    }

    private Drink findNotReadyDrink() {
        for (int i = 0; i < orderQueue.size(); i++) {
            Drink nextDrink = orderQueue.get(i);

            if (!nextDrink.isReady()) {
                currentDrinkId = i;
                return nextDrink;
            }
        }

        currentDrinkId = -1;
        return null;
    }

    /**
     * Sprawdzamy ile jest wszystkich drinków - do zrobienia + zrobionych.
     *
     * @return
     */
    private int checkOrderQueueSize() {
        synchronized (orderQueue) {
            return orderQueue.size();
        }
    }

}

