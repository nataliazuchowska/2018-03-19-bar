package pl.codementors.bar;

public class Drink {

    private String name;
    private boolean isReady = false;

    public Drink(String name) {
        this.name = name;
    }

    public String name() {
        return name;
    }

    public boolean isReady() {
        return isReady;
    }

    public void markAsReady() {
        isReady = true;
    }
}